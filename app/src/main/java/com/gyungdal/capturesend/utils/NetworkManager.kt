package com.gyungdal.capturesend.utils

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.util.*




/**
 * Created by 9274a on 2017-07-10.
 */

class NetworkManager : AsyncTask<Unit, Unit, Unit> {
    private val serverURL : String = "http://gyungdal.iptime.org:3141/OCR"
    private val fileName : String
    private val context : Context

    constructor(context : Context, fileName : String){
        this.context = context
        this.fileName = fileName
    }

    private fun getByteArrayFromFile() : ByteArray{
        val result : MutableList<Byte> = mutableListOf()

        val fis = context.openFileInput(fileName)
        var temp = ByteArray(16384)
        while((fis.read(temp, 0, 16384)) != -1){
            result.addAll(temp.toTypedArray())
            Arrays.fill(temp, 0)
        }
        fis.close()
        return result.toByteArray()
    }

    override fun doInBackground(vararg p0: Unit?) {
        try {

            val array : ByteArray = android.util.Base64.encode(getByteArrayFromFile(), android.util.Base64.DEFAULT)

            val doc : Document = Jsoup.connect(serverURL)
                    .data("image", String(array))
                    .timeout(20000)
                    .ignoreHttpErrors(true)
                    .ignoreContentType(true)
                    .validateTLSCertificates(false)
                    .post()

            Log.i("Server Communicate", doc.toString())
            Toast.makeText(context, doc.text(), Toast.LENGTH_SHORT).show()

        }catch(e : Exception){
            Log.e("Server Communicate", e.message)
        }
    }

}