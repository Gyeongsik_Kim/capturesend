package com.gyungdal.capturesend.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.PixelFormat
import android.graphics.Point
import android.hardware.display.DisplayManager
import android.hardware.display.VirtualDisplay
import android.media.Image
import android.media.ImageReader
import android.media.projection.MediaProjection
import android.media.projection.MediaProjectionManager
import android.os.*
import android.util.DisplayMetrics
import android.util.Log
import android.view.Display
import android.view.OrientationEventListener
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import com.gyungdal.capturesend.floating.FloatingWindow

import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStreamWriter
import java.nio.ByteBuffer
import java.util.Timer
import java.util.TimerTask


/**
 * Created by 9274a on 2017-07-10.
 */

class ScreenshotManager : Activity() {
    //상수
    private val TAG = ScreenshotManager::class.java.name
    private val REQUEST_CODE = 100
    private var STORE_DIRECTORY: String? = null
    private var IMAGES_PRODUCED: Int = 0
    private val SCREENCAP_NAME = "screencap"
    private val VIRTUAL_DISPLAY_FLAGS = DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY or DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC
    private var sMediaProjection: MediaProjection? = null


    private var mProjectionManager: MediaProjectionManager? = null
    private var mImageReader: ImageReader? = null
    private var mHandler: Handler? = null
    private var mDisplay: Display? = null
    private var mVirtualDisplay: VirtualDisplay? = null
    private var mDensity: Int = 0
    private var mWidth: Int = 0
    private var mHeight: Int = 0
    private var mRotation: Int = 0
    private var mOrientationChangeCallback: OrientationChangeCallback? = null

    private inner class ImageAvailableListener : ImageReader.OnImageAvailableListener {
        override fun onImageAvailable(reader: ImageReader) {
            var image: Image? = null
            var fos: FileOutputStream? = null
            var bitmap: Bitmap? = null

            try {
                image = reader.acquireLatestImage()
                if (image != null) {
                    val planes = image.planes
                    val buffer = planes[0].buffer
                    val pixelStride = planes[0].pixelStride
                    val rowStride = planes[0].rowStride
                    val rowPadding = rowStride - pixelStride * mWidth
                    //val file = File(STORE_DIRECTORY + "myscreen_" + IMAGES_PRODUCED + ".png")
                    //if(file.exists())
                    //    file.deleteOnExit()

                    bitmap = Bitmap.createBitmap(mWidth + rowPadding / pixelStride, mHeight, Bitmap.Config.ARGB_8888)
                    bitmap!!.copyPixelsFromBuffer(buffer)
                    //Log.i("SVAE POS", STORE_DIRECTORY + "myscreen_" + IMAGES_PRODUCED + ".png")
                    fos = openFileOutput("test.png", Context.MODE_PRIVATE)
                    bitmap.compress(CompressFormat.JPEG, 100, fos)
                    Log.i(TAG, applicationContext.cacheDir.absolutePath)
                    Log.e(TAG, "captured image: " + IMAGES_PRODUCED)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                if (fos != null) {
                    try {
                        fos.close()
                    } catch (ioe: IOException) {
                        ioe.printStackTrace()
                    }

                }

                if (bitmap != null) {
                    bitmap.recycle()
                }

                if (image != null) {
                    image.close()
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        startService(Intent(this, FloatingWindow::class.java))
    }
    private inner class OrientationChangeCallback internal constructor(context: Context) : OrientationEventListener(context) {

        override fun onOrientationChanged(orientation: Int) {
            val rotation = mDisplay!!.rotation
            if (rotation != mRotation) {
                mRotation = rotation
                try {
                    // clean up
                    if (mVirtualDisplay != null) mVirtualDisplay!!.release()
                    if (mImageReader != null) mImageReader!!.setOnImageAvailableListener(null, null)

                    // re-create virtual display depending on device width / height
                    createVirtualDisplay()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }

    private inner class MediaProjectionStopCallback : MediaProjection.Callback() {
        override fun onStop() {
            Log.e("ScreenCapture", "stopping projection.")
            mHandler!!.post {
                if (mVirtualDisplay != null) mVirtualDisplay!!.release()
                if (mImageReader != null) mImageReader!!.setOnImageAvailableListener(null, null)
                if (mOrientationChangeCallback != null) mOrientationChangeCallback!!.disable()
                sMediaProjection!!.unregisterCallback(this@MediaProjectionStopCallback)
            }
        }
    }

    /****************************************** Activity Lifecycle methods  */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        stopService(Intent(this, FloatingWindow::class.java))
        // call for the projection manager
        mProjectionManager = getSystemService(Context.MEDIA_PROJECTION_SERVICE) as MediaProjectionManager
        startProjection()
        //


        // start capture handling thread
        object : Thread() {
            override fun run() {
                Looper.prepare()
                mHandler = Handler()
                Looper.loop()
            }
        }.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == REQUEST_CODE) {
            sMediaProjection = mProjectionManager!!.getMediaProjection(resultCode, data)

            if (sMediaProjection != null) {
                //STORE_DIRECTORY = Environment.DIRECTORY_PICTURES + File.separator
                ////STORE_DIRECTORY = "/sdcard/"
                //Log.i("ROOT PATH", STORE_DIRECTORY)
                //val storeDirectory = File(STORE_DIRECTORY!!)
                //if (!storeDirectory.exists()) {
                //    val success = storeDirectory.mkdirs()
                //    i f (!success) {
                //        Log.e(TAG, "failed to create file storage directory.")
                //        return
                //    }
                //}


                // display metrics
                val metrics = resources.displayMetrics
                mDensity = metrics.densityDpi
                mDisplay = windowManager.defaultDisplay

                // create virtual display depending on device width / height
                createVirtualDisplay()

                // register orientation change callback
                mOrientationChangeCallback = OrientationChangeCallback(this)
                if (mOrientationChangeCallback!!.canDetectOrientation()) {
                    mOrientationChangeCallback!!.enable()
                }

                // register media projection stop callback
                sMediaProjection!!.registerCallback(MediaProjectionStopCallback(), mHandler)
            }

            Timer().schedule(object : TimerTask() {
                override fun run() {
                    stopProjection()
                    NetworkManager(context = applicationContext, fileName = "test.png")
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                    finish()
                }
            }, 1000)
        }
    }

    private fun startProjection() {
        startActivityForResult(mProjectionManager!!.createScreenCaptureIntent(), REQUEST_CODE)
    }

    private fun stopProjection() {
        mHandler!!.post {
            if (sMediaProjection != null) {
                sMediaProjection!!.stop()
            }
        }
    }

    private fun createVirtualDisplay() {
        // get width and height
        val size = Point()
        mDisplay!!.getSize(size)
        mWidth = size.x
        mHeight = size.y

        // start capture reader
        mImageReader = ImageReader.newInstance(mWidth, mHeight, PixelFormat.RGBX_8888, 2)
        mVirtualDisplay = sMediaProjection!!.createVirtualDisplay(SCREENCAP_NAME, mWidth, mHeight, mDensity, VIRTUAL_DISPLAY_FLAGS, mImageReader!!.surface, null, mHandler)
        mImageReader!!.setOnImageAvailableListener(ImageAvailableListener(), mHandler)
    }

}
