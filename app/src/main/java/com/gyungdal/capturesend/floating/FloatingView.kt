package com.gyungdal.capturesend.floating

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.View

/**
 * Created by 9274a on 2017-07-10.
 */

class FloatingView : View {

    constructor(context : Context) : super(context)

    constructor(context : Context, attr : AttributeSet) : super(context, attr)

    private fun dpToPx(dp: Int): Float {
        val displayMetrics = context!!.resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)) + 0.01F
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        val paint : Paint = Paint()
        paint.color = Color.BLUE
        canvas!!.drawOval(RectF(0F, 0F,  dpToPx(100), dpToPx(100)), paint)

    }
}
