package com.gyungdal.capturesend.floating

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.IBinder
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.Button

import com.gyungdal.capturesend.R
import com.gyungdal.capturesend.MainActivity
import com.gyungdal.capturesend.utils.ScreenshotManager
import android.util.DisplayMetrics




/**
 * Created by 9274a on 2017-07-10.
 */

class FloatingWindow : Service() {

    private var context: Context? = null

    private var flagClick: Boolean = false
    private var flagView: Boolean = false

    private var testView: FloatingView? = null
    private var inflateView: View? = null
    private var wm: WindowManager? = null
    private var params: WindowManager.LayoutParams? = null
    private var floatWindowParams: WindowManager.LayoutParams? = null

    private var start_x: Int = 0
    private var start_y: Int = 0
    private var prev_x: Int = 0
    private var prev_y: Int = 0


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun dpToPx(dp: Int): Int {
        val displayMetrics = context!!.resources.displayMetrics
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate() {
        super.onCreate()
        context = this

        testView = FloatingView(this)
        testView!!.setOnTouchListener(onTouchListener)
        val size = dpToPx(100)
        Log.i("CIRCLE SIZE", "" + size)
        params = WindowManager.LayoutParams(
                size,
                size,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, //터치 인식
                PixelFormat.TRANSLUCENT) //투명

        params!!.gravity = Gravity.LEFT or Gravity.TOP
        wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm!!.addView(testView, params)

        inflateView = View.inflate(this, R.layout.floattingwindow, null)
        floatWindowParams = WindowManager.LayoutParams(
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, //터치 인식
                PixelFormat.TRANSLUCENT) //투명


    }

    private val onTouchListener = View.OnTouchListener { _, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                flagClick = true

                val x = event.rawX.toInt()
                val y = event.rawY.toInt()

                start_x = x
                start_y = y
                prev_x = params!!.x
                prev_y = params!!.y
            }
            MotionEvent.ACTION_MOVE -> {
                flagClick = false
                val x = (event.rawX - start_x).toInt()    //이동한 거리
                val y = (event.rawY - start_y).toInt()    //이동한 거리
                params!!.x = prev_x + x
                params!!.y = prev_y + y
                wm!!.updateViewLayout(testView, params)

            }
            MotionEvent.ACTION_UP -> {
                if (flagClick) {
                    floatWindowParams!!.x = params!!.x + 70
                    floatWindowParams!!.y = params!!.y - 50
                    Log.v("@@@@@@@@@", "@@@@@@ floatWindowParams.x: " + floatWindowParams!!.x + " floatWindowParams.y: " + floatWindowParams!!.y)

                    inflateView = View.inflate(context, R.layout.floattingwindow, null)
                    inflateView!!.setOnClickListener {
                        (getSystemService(Context.WINDOW_SERVICE) as WindowManager).removeView(inflateView)
                        inflateView = null
                    }
                    val btn = inflateView!!.findViewById<View>(R.id.btnend) as Button
                    btn.x = floatWindowParams!!.x.toFloat()
                    btn.y = (floatWindowParams!!.y + 50).toFloat()
                    btn.setOnClickListener {
                        onDestroy()
                        if (inflateView != null)
                        //서비스 종료시 뷰 제거. *중요 : 뷰를 꼭 제거 해야함.
                        {
                            (getSystemService(Context.WINDOW_SERVICE) as WindowManager).removeView(inflateView)
                            inflateView = null
                        }
                    }

                    val bt = inflateView!!.findViewById<View>(R.id.btn) as Button
                    bt.x = floatWindowParams!!.x.toFloat()
                    bt.y = floatWindowParams!!.y.toFloat()
                    bt.setOnClickListener {
                        val mainIntent = Intent(baseContext, ScreenshotManager::class.java)
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                        startActivity(mainIntent)
                        if (inflateView != null)
                        {
                            (getSystemService(Context.WINDOW_SERVICE) as WindowManager).removeView(inflateView)
                            inflateView = null
                        }
                    }
                    wm!!.addView(inflateView, floatWindowParams)
                    flagView = true
                }
            }
        }
        false
    }

    override fun onDestroy() {
        super.onDestroy()
        if (testView != null)
        //서비스 종료시 뷰 제거. *중요 : 뷰를 꼭 제거 해야함.
        {
            (getSystemService(Context.WINDOW_SERVICE) as WindowManager).removeView(testView)
            testView = null
        }
        if (inflateView != null)
        //서비스 종료시 뷰 제거. *중요 : 뷰를 꼭 제거 해야함.
        {
            if (flagView) {
                (getSystemService(Context.WINDOW_SERVICE) as WindowManager).removeView(inflateView)
            }
            inflateView = null
        }
        stopSelf()
    }
}